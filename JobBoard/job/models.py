from django.db import models

# Create your models here.


class JobOffer(models.Model):
    company_name = models.CharField(max_length=20) #강의에서는 50
    company_email = models.EmailField(max_length=120) #강의에서는 max_length 작성하지 않음
    job_title = models.CharField(max_length=20) #강의에서는 60
    job_description = models.TextField(max_length=200)
    salary = models.FloatField() #강의에서는 PositiveIntegerField()로 작성
    city = models.CharField(max_length=20) #강의에서는 35
    state = models.CharField(max_length=20) #강의에서는 35
    available = models.BooleanField(default=True)
    created_at = models.DateField(auto_now_add=True)
    # 나는 깜빡잊고 created_at을 빼먹었다 핫..


    def __str__(self):
        return f"{ self.company_name } { self.job_title }" 
        # 강의에서는 return self.company_name이라고 했지만 나는 타이틀까지 주고 싶었다